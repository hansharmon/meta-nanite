SUMMARY = "Adding a user"
DESCRIPTION ="This recipes shows how to add a user and change the root password for yocto"
SECTION = "users"
PR = "r1"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

S = "${WORKDIR}"


#PACKAGES += "${PN}"
# # Change Root Password
inherit useradd

# Update root password
# EXTRA_USER_PARAMS = "\
#     usermod -p '\$1\$DFmZX7O7\$dicawL5q5oE6Kg9GTBXmH/' root; \
#     "

# # You must set USERADD_PACKAGES when you inherit useradd. This
# # lists which output packages will include the user/group
# # creation code.
USERADD_PACKAGES = "${PN}"

USERADD_PARAM:${PN} = " -d /home/nanite -r -s /bin/bash -p '\$1\$usa3yCqc\$PFiRR7SPsMPvruCHqZnqT.' -G root,sudo,shutdown nanite"

do_install:append () {
    install -d -m 755 ${D}/home/nanite
    chown -R nanite ${D}/home/nanite
}

FILES:${PN} = "/home/nanite"
INHIBIT_PACKAGE_DEBUG_SPLIT = "1"

