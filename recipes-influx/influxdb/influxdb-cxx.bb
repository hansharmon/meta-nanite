DESCRIPTION="InfluxDB C++ wrappers"
LICENSE="CLOSED"

DEPENDS += "\
    curl        \
"

inherit cmake
SRC_URI="git://github.com/awegrzyn/influxdb-cxx.git;branch=master;protocol=https"
SRCREV = "${AUTOREV}"

DEPENDS += "\
    curl    \
"

S = "${WORKDIR}/git"
BBCLASSEXTEND = "native nativesdk"

INSANE_SKIP:${PN}:append = " ldflags file-rdeps libdir "
INHIBIT_PACKAGE_STRIP = "1"
INHIBIT_PACKAGE_DEBUG_SPLIT = "1"
SOLIBS = ".so"
FILES_SOLIBSDEV = ""

