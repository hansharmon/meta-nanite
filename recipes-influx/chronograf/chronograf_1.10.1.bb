require chronograf.inc

PR = "r1"

LICENSE = "CLOSED"

SRC_URI = "https://dl.influxdata.com/chronograf/releases/chronograf-1.10.1_linux_armhf.tar.gz"
SRC_URI[sha256sum] = "f43796d30dd72df6fb9a4fe9390ce602e01b3c8400b342eda58f28928de7c6ae"


S = "${WORKDIR}/chronograf-1.10.1-1"