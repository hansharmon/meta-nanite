#!/bin/bash
set -x

# If the network does not exit on the data drive copy from defaults.
if  [ ! -d /data/nanite ]; then
        mkdir /data/nanite
        cp /defaults/nanite/* /data/nanite
        cd /data/nanite/
        tar -xzf nanite.web.tar.gz
fi
cd /data/nanite
/usr/bin/nanite nanite.json
