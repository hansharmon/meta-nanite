FILESEXTRAPATHS:prepend := "${THISDIR}/files:"
LICENSE = "CLOSED"
inherit systemd

SYSTEMD_AUTO_ENABLE = "enable"
SYSTEMD_SERVICE:${PN} = "nanite.service"

RDEPENDS:${PN} += "bash"
INSANE_SKIP:${PN}:append = "installed-vs-shipped"

SRC_URI:append = " file://nanite.service \
                   file://nanite_start.sh \
                   file://nanite.json \
                   file://nanite.web.tar.gz;unpack=0 \
                   "
FILES:${PN} = "${systemd_unitdir}/system/nanite.service \
               /defaults/nanite/nanite.web.tar.gz \
               /defaults/nanite/nanite.json   \ 
               /usr/bin/nanite_start.sh \
               "

do_install() {
  install -d ${D}/${systemd_unitdir}/system
  install -m 0644 ${WORKDIR}/nanite.service ${D}/${systemd_unitdir}/system

  mkdir -p ${D}/usr/bin
  cp -f ${WORKDIR}/nanite_start.sh ${D}/usr/bin/nanite_start.sh
  chmod +x ${D}/usr/bin/nanite_start.sh

  install -d ${D}/defaults/nanite
  mkdir -p ${D}/defaults/nanite
  install -m 0666 ${WORKDIR}/nanite.json ${D}/defaults/nanite
  install -m 0666 ${WORKDIR}/nanite.web.tar.gz ${D}/defaults/nanite
} 


