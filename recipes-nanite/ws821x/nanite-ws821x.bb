LICENSE = "CLOSED"

DEPENDS += "     \
    nanite-core  \
    rpi-ws8211   \
"

SRC_URI = "git://bitbucket.org/hansharmon/nanite_ws821x.git;protocol=https;branch=master"

# Modify these as desired
PV = "1.0+git${SRCPV}"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"

inherit cmake

# Specify any options you want to pass to cmake using EXTRA_OECMAKE:
EXTRA_OECMAKE += " -DCMAKE_BUILD_TYPE="Release" "
INSANE_SKIP:${PN}:append = "already-stripped"
SOLIBS = ".so"
FILES_SOLIBSDEV = ""
