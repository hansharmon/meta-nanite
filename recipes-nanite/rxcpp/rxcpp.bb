SUMMARY = "Reactive Extension for Native C++ (RxCpp)"
HOMEPAGE = "http://reactivex.io/RxCpp/"
SECTION = "libs"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://license.md;md5=e9e35c9e12d881a54ee5f5c7830f6b78"

SRC_URI = "git://github.com/ReactiveX/RxCpp.git;branch=main;protocol=https"

SRCREV = "${AUTOREV}"

#PV = "1.1.0+git${SRCPV}"

S = "${WORKDIR}/git"

inherit cmake

# the install path for cmake modules etc. is hardcoded as ${prefix}/lib in
# CMakeLists.txt, which breaks the package split with multilib
EXTRA_OECMAKE += "-DLIB_INSTALL_DIR=${libdir}"

# RxCpp is a header-only C++ library, so the main package will be empty.

ALLOW_EMPTY:${PN} = "1"

BBCLASSEXTEND = "native nativesdk"


