LICENSE = "CLOSED"

DEPENDS += "    \
    file        \
    apr         \
    apr-util    \
    openssl     \
"

BRANCH = "3.9.x"
SRCREV = "fcbe30b1923f1a9565e6ae39d034d410fafe32c6"
SRC_URI = "git://gitbox.apache.org/repos/asf/activemq-cpp;protocol=https;branch=${BRANCH}"

# Modify these as desired

# Change to the correct directory for auto-tools to work.
S = "${WORKDIR}/git/activemq-cpp"

inherit autotools

# THis gets ignored, somehow need to find the right magic here to make this work, error message is unclear on the actual error.
# INSANE_SKIP:${PN}:append = " configure-unsafe "


