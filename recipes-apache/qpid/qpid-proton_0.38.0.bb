# 0.34.0 is the last non-swig complaining version.
LICENSE = "CLOSED"

DEPENDS += "    \
    apr         \
    openssl     \
"

BRANCH = "main"
SRCREV = "da60728ebc92f40f265c13b4a4f55cb63265cd26"
SRC_URI = "git://github.com/apache/qpid-proton;protocol=https;branch=${BRANCH}"

# Modify these as desired

S = "${WORKDIR}/git"

inherit cmake

# Specify any options you want to pass to cmake using EXTRA_OECMAKE:
EXTRA_OECMAKE += " -DBUILD_PYTHON=OFF -DBUILD_PHP=OFF -DBUILD_PERL=OFF -DBUILD_RUBY=OFF -DBUILD_JAVA=OFF -DBUILD_TESTING=OFF -U PYTHON_EXECUTABLE "


# Need to ignore that the examples are not packaged
# There should be a cleaner way to do this rather than ignoring the QA warning.
INSANE_SKIP:${PN}:append = " installed-vs-shipped "
