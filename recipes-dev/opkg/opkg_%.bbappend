FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += " file://opkg.conf "

do_install:append(){
	install -d ${D}/etc/opkg/
	install -m 0644 ${WORKDIR}/opkg.conf ${D}/etc/opkg
}

# Added due to sdk failing to build. 
FILES:${PN} += " \
    ${libdir} \
    ${bindir} \
	/etc/opkg \		
"