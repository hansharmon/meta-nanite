LICENSE = "MIT"

inherit core-image

##########################
# Paritioning
# This section is for setting up partitions
WKS_FILE="nanite.wks.in"
IMAGE_INSTALL += " grow-data-part "


#Remove Splash Screen.
IMAGE_FEATURE:remove = "psplash"
IMAGE_FEATURE:remove = "splash"
DISTRO_FEATURES:append = " wifi "
#DISTRO_FEATURES:append = " x11 opengl "


# Pi Options Do these need to be in a local.conf or should be make a meta-hans-pi???
DISABLE_RPI_BOOT_LOGO="1"
DISABLE_SPLASH="1"
# TODO:  We may need to adjust this to get wifi to be consistant?
BOOT_DELAY="5"

VIDEO_CAMERA="1"
ENABLE_SPI_BUS="1"

ENABLE_I2C="1"
KERNEL_MODULE_AUTOLOAD_rpi += "i2c-dev i2c-bcm2708"

ENABLE_UART="1"

# Setup for rpi-sdimage and switch to ext4
IMAGE_FSTYPES="wic.bz2 tar.gz ext4.gz"
SDIMG_ROOTFS_TYPE="ext4"

# SSHD
EXTRA_IMAGE_FEATURES = "ssh-server-openssh"

#####################
# UBoot - For SWU
RPI_USE_U_BOOT = "1"
PREFERRED_PROVIDER_u-boot-fw-utils = "libubootenv"
PREFERRED_PROVIDER_u-boot-default-script = "rpi-u-boot-scr"
# SWU attempt
CORE_IMAGE_EXTRA_INSTALL += " swupdate "
CORE_IMAGE_EXTRA_INSTALL += " swupdate-www "

# Check config.txt for these
# GROUP  1 -> CEA, 2 -> DMT
HDMI_GROUP = "1"
# Mode 4 -> 720p
HDMI_MODE = "2"
# Allow this to boot without needing HDMI
HDMI_FORCE_HOTPLUG = "1"
HDMI_DRIVE="1"

################
## Remove the splash screen
# IMAGE_FEATURES:remove = " psplash splash "

#####################
# Add our user(s)
IMAGE_INSTALL += " users-nanite "

# - I dont this is working correctly, but the user-nanite is so ignore for now.
inherit extrausers
EXTRA_USER_PARAMS = "\
    usermod -p '\$1\$usa3yCqc\$PFiRR7SPsMPvruCHqZnqT.' root; \
    "

#######################
# Networking/Wifi
# Enable WiFi
IMAGE_INSTALL += " networkmanager "
IMAGE_INSTALL += " networkmanager-bash-completion "
IMAGE_INSTALL += " networkmanager-nmcli "
IMAGE_INSTALL += " link-network "

########################
# SSHD
IMAGE_INSTALL += " \  
    openssh \
    openssl \
    openssh-sftp-server "

########################
# Bluetooth
DISTRO_FEATURES:append = " bluez5 bluetooth "
IMAGE_INSTALL += "\
    pi-bluetooth                    \
"

########################
# Software packages.
###
# Useful base utilities.
# Useful tools
IMAGE_INSTALL += "   \
    apt              \
    at               \
    attr             \
    bash             \
    bc               \
    bzip2            \
    can-utils        \
    coreutils        \
    cronie           \
    curl             \ 
    dos2unix         \
    diffutils        \
    dosfstools       \
    e2fsprogs        \
    file             \
    findutils        \
    htop             \
    i2c-tools        \
    iproute2         \
    iptables         \
    iputils          \
    gawk             \
    grep             \
    gzip             \
    libgpiod         \
    libgpiod-tools   \
    libiio           \
    libiio-tests     \
    libusb1          \
    libftdi          \
    logrotate        \
    m4               \
    makedevs         \
    mc               \
    minicom          \
    module-init-tools\
    mmc-utils        \
    ncurses          \
    nano             \
    net-tools        \
    ntp              \
    openssl          \
    opkg             \
    parted           \
    patch            \
    procps           \
    psmisc           \
    sed              \
    smartmontools    \
    sudo             \ 
    tar              \
    tcpdump          \
    time             \
    usbutils         \
    util-linux       \ 
    util-linux-lsblk \
    wget             \
    which            \
    "


##########################
# SWUUPDATE
# This section is also for getting swupdate to work with those partitions
# Tools for communicating with uboot environment
# TODO:  Need to copy this recipe out of another project.
# IMAGE_INSTALL += " u-boot-fw-tools "
CORE_IMAGE_EXTRA_INSTALL:append = " u-boot-fw-utils "
IMAGE_INSTALL += " libubootenv "
IMAGE_INSTALL += " swupdate "
IMAGE_INSTALL += " swupdate-www "
IMAGE_INSTALL += " swupdate-tools "
IMAGE_INSTALL += " lua "

################
# Package Management
IMAGE_INSTALL += "\
    opkg          \
    wget          \
"
IMAGE_FEATURES += " package-management "
#PACKAGE_CLASSES ?= "package_ipk"

IMAGE_FEATURES += " hwcodecs "

# # Libraries for code.
# IMAGE_INSTALL += "  \
#     boost           \
#     czmq            \
#     cppzmq-dev      \
#     curl            \
#     libftdi         \
#     libmodbus       \
#     log4cpp         \
#     protobuf        \
#     protobuf-c      \
#     rapidjson       \
#     sqlite3         \
# "   

