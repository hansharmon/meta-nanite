require image-nanite.bb

##################
# Dev tools
EXTRA_IMAGE_FEATURES = " tools-sdk dev-pkgs "
IMAGE_INSTALL += " libstdc++ mtd-utils "
IMAGE_INSTALL += " packagegroup-core-buildessential"
IMAGE_INSTALL += " \
    autoconf       \
    automake       \
    cmake          \
    doxygen        \
    g++            \
    gcc            \
    gdb            \
    git            \
    go             \
    lcov           \
    ninja          \
    maven          \
    python3        \
"

