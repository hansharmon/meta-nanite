###############
# Next Level Beyond DEV
require image-nanite-dev.bb

########################
# Image Processing
IMAGE_INSTALL += " opencv "

###############
# Web Front End Management
IMAGE_INSTALL += " nginx "

################
# Time series database and dashboards
# This is almost too costly.
IMAGE_INSTALL +=  " influxdb "
# These end up taking too many resources for a pi-zero-w.
#IMAGE_INSTALL += " telegraf chronograf kapacitor " 

#############
# Databases
IMAGE_INSTALL += " sqlite3 "

#############
# Communications Layers
IMAGE_INSTALL += "\
 libmodbus        \
 librdkafka       \
 mosquitto        \
 paho-mqtt-c      \
 paho-mqtt-cpp    \
 apr              \
"

####################
# File Browswer
IMAGE_INSTALL += " filebrowser " 

####################
# Cockpit
# Nanite dash board is getting good, this may not be necessary soon.
IMAGE_INSTALL += " cockpit "
IMAGE_INSTALL += " cockpit-bridge "
IMAGE_INSTALL += " cockpit-dashboard "
# IMAGE_INSTALL += " cockpit-desktop "
# IMAGE_INSTALL += " cockpit-docker "
IMAGE_INSTALL += " cockpit-kdump "
IMAGE_INSTALL += " cockpit-networkmanager "
#  IMAGE_INSTALL += " cockpit-machines "
IMAGE_INSTALL += " cockpit-pcp "
IMAGE_INSTALL += " cockpit-playground "
IMAGE_INSTALL += " cockpit-selinux "
IMAGE_INSTALL += " cockpit-shell "
IMAGE_INSTALL += " cockpit-sosreport "
IMAGE_INSTALL += " cockpit-systemd "
IMAGE_INSTALL += " cockpit-tuned "
IMAGE_INSTALL += " cockpit-users "
IMAGE_INSTALL += " cockpit-ws "

#########################
# Automation
# TODO: Revisit Node-Red requires a LOT of resources.
# This is likely to be replace by nanite-flow and nanite-web as this it just too heavy.
# This is a no-go on a Pi-Zero-W
# IMAGE_INSTALL += " node-red "


###############################
# Problem Children here.
# grafana is likely a no go for now for a pi0 anyway.
# Being replaced by nanite-web, which is NOT grafana, but works pretty well.
# This is a no-go on a Pi-Zero-W
# IMAGE_INSTALL += " nodejs-oe-cache-native "
# IMAGE_INSTALL += " grafana "


################################
# Nanite
IMAGE_INSTALL += "\
    nanite-amqp     \
    nanite-core     \
    nanite-dht      \
    nanite-influx   \
    nanite-io       \
    nanite-joystick \
    nanite-kafka    \
    nanite-modbus   \
    nanite-motors   \
    nanite-mqtt     \
    nanite-service  \
    nanite-video    \
    nanite-ws821x   \
    nanite-zmq      \
"

##############################
# Web Browser 
# This might have been a bit of a stretch goal.
# This is also breaking swupdate somehow.
# This is a no-go on a Pi-Zero-W
# Going headless with a combination of NGINX and other application to drive a desktop experience.
# A true UX through the HDMI might been cool down the road though.
#IMAGE_INSTALL:append = " chromium-x11 "
#IMAGE_FEATURES:append = " x11-base hwcodecs "
#PACKAGECONFIG:append:pn-chromium-x11 = " kiosk-mode "



