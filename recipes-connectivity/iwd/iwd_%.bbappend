FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += "file://Starlink.psk"
SRC_URI += "file://main.conf"

do_install:append() {
    install -d ${D}/var/lib/iwd/
    install -d ${D}/etc/iwd/

    install -m 0644 ${WORKDIR}/Starlink.psk ${D}/var/lib/iwd/
    install -m 0644 ${WORKDIR}/main.conf ${D}/etc/iwd
}
FILES:${PN} += "/var/lib/iwd/Starlink.psk"
