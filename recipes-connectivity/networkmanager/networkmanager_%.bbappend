FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += "file://Starlink.nmconnection"

do_install:append() {
    install -d ${D}/etc/NetworkManager/system-connections

    install -m 0600 ${WORKDIR}/Starlink.nmconnection ${D}/etc/NetworkManager/system-connections
}
FILES:${PN} += "/etc/NetworkManager/system-connections/Starlink.nmconnection"
