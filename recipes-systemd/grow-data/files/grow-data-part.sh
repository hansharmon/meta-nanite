#!/bin/bash
set -xe
DATAPARTINIT=$( lsblk | grep 128M )
DATAPART=$( lsblk | grep data )


if [ -z "$DATAPARTINIT" ]; then
	umask 000 /data
	chmod 777 /data
	echo "Data partition: ${DATAPART}"
else
	echo "Resizing data partition to fill eMMC"	
    /bin/umount -l /data
	/usr/sbin/parted --script /dev/mmcblk0 resizepart 4 100%
	/usr/sbin/mkfs.vfat /dev/mmcblk0p4
    /bin/mount /dev/mmcblk0p4 /data
	chmod 777 /data
	umask 000 /data
	#/lib/systemd/systemd-growfs /data    
    /sbin/reboot
	echo "data resize complete"
fi







