LICENSE = "CLOSED"
inherit systemd

SYSTEMD_AUTO_ENABLE = "enable"
SYSTEMD_SERVICE:${PN} = "wifi.service"

SRC_URI:append = " file://wifi.service "
FILES:${PN} = "${systemd_unitdir}/system/wifi.service"

do_install() {
  install -d ${D}/${systemd_unitdir}/system
  install -m 0644 ${WORKDIR}/wifi.service ${D}/${systemd_unitdir}/system
}
