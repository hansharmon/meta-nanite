#!/bin/bash
set -x

systemctl stop NetworkManager
# If the network does not exit on the data drive copy from defaults.
if  [ ! -d /data/network ]; then
        mkdir /data/network
        cp /defaults/wlan.network /data/network
        cp /defaults/eth.network /data/network
fi
rm /etc/systemd/network/*
cp /data/network/* /etc/systemd/network/

# If the system-connections does not exit on the data drive copy from defaults.
if  [ ! -d /data/wifi ]; then
        mkdir /data/wifi
        cp /defaults/Starlink.nmconnection /data/wifi
fi
rm /etc/NetworkManager/system-connections/*
cp /data/wifi/* /etc/NetworkManager/system-connections
chmod 0600 /etc/NetworkManager/system-connections/*
chown root /etc/NetworkManager/system-connections/*

# Restart the network controller
#networkctl reload
systemctl start NetworkManager
