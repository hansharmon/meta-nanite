FILESEXTRAPATHS:prepend := "${THISDIR}/files:"
LICENSE = "CLOSED"
inherit systemd

SYSTEMD_AUTO_ENABLE = "enable"
SYSTEMD_SERVICE:${PN} = "link-network.service"

RDEPENDS:${PN} += "bash"

SRC_URI:append = " file://link-network.service   \
                   file://link-network.sh        \
                   file://eth.network            \
                   file://wlan.network           \
                   file://Starlink.nmconnection  \
                   file://Starlink2.nmconnection   \
                   "
FILES:${PN} = "${systemd_unitdir}/system/link-network.service \
               /usr/bin/link-network.sh         \
               /defaults/eth.network            \
               /defaults/wlan.network           \
               /defaults/Starlink.nmconnection  \
               /defaults/Starlink2.nmconnection   \
               "

do_install() {
  install -d ${D}/${systemd_unitdir}/system
  install -m 0644 ${WORKDIR}/link-network.service ${D}/${systemd_unitdir}/system

  mkdir -p ${D}/usr/bin
  cp -f ${WORKDIR}/link-network.sh ${D}/usr/bin/link-network.sh
  chmod +x ${D}/usr/bin/link-network.sh

  install -d ${D}/defaults
  mkdir -p ${D}/defaults
  install -m 0666 ${WORKDIR}/eth.network ${D}/defaults
  install -m 0666 ${WORKDIR}/wlan.network ${D}/defaults
  install -m 0666 ${WORKDIR}/Starlink.nmconnection ${D}/defaults
  install -m 0666 ${WORKDIR}/Starlink2.nmconnection ${D}/defaults

} 


