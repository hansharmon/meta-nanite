// To Build:
// g++ opencv_test.cpp -lopencv_video -lopencv_videoio -lopencv_core -lopencv_imgcodecs -I/usr/include/opencv4 -o opencv_test
// Exucute:
// sudo ./opencv_test -1
#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/imgcodecs.hpp>

#include <iostream>
#include <stdlib.h>
#include <string>

using namespace cv;
using namespace std;

int main (int argc, char * argv[]) {
    cv::Mat          frame;       //!< Last frame read.
    cv::VideoCapture vidcap;      //!< Open CV's capture
    int id = 0;

    if (argc > 1) {
        id = std::stoi(argv[1]);
    }

    std::cout << "Opening " << id << std::endl;
    vidcap.open(id + CAP_ANY);
    if (!vidcap.isOpened()) {
        cout << "Cannot Open Video " + to_string(id) << endl;
        return -1;
    }

    std::string name = "frame.jpg";
    vidcap.read(frame);
    
    // vector<unsigned char> buffer;
    // vector<int> params = { ImwriteFlags::IMWRITE_JPEG_QUALITY, 80 };
    // imencode(".jpg",frame,buffer,params);         // JPEG in memory
    imwrite(name.c_str(),frame);
    return 0;
}