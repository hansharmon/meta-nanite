# Open CV Captures
# Example Code for a simple capture program and simple how to compile bb.
FILESEXTRAPATHS:prepend := "${THISDIR}/files:"
DESCRIPTION = "OpenCV Image Captures a Single Frame"
SECION = "opencv"
LICENSE="CLOSED"

DEPENDS += "    \
    opencv      \
    "
INSANE_SKIP:${PN} += "ldflags"

SRC_URI="file://opencv_capture.cpp"

S = "${WORKDIR}"

do_compile() {
    ${CXX} opencv_capture.cpp -o opencv_capture -lopencv_core -lopencv_imgcodecs -lopencv_videoio -I/usr/include/opencv4
}

do_install() {
    install -d ${D}${bindir}
    install -m 0755 opencv_capture ${D}${bindir}
}


