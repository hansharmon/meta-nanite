FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += " file://nginx.conf \
			file://nginx.cert.pem \
			file://nginx.key.pem \
"

do_install:append(){
	install -d ${D}/etc/nginx/
	install -m 0644 ${WORKDIR}/nginx.conf ${D}/etc/nginx

	install -d ${D}/etc/nginx/ssl
	install -m 0644 ${WORKDIR}/nginx.cert.pem ${D}/etc/nginx/ssl
	install -m 0644 ${WORKDIR}/nginx.key.pem ${D}/etc/nginx/ssl
}

FILES:${PN} += " \
	/etc/nginx \		
	/etc/nginx/ssl \		
"
