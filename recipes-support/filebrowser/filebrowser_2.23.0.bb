FILESEXTRAPATHS:prepend = "${THISDIR}/files:"
DESCRIPTION = "Web based file explorer"
LICENSE="CLOSED"
#LICENSE = "Apache-2.0"
#LIC_FILES_CHKSUM = "file://src/github.com/filebrowser/filebrowser/LICENSE;md5=31f6db4579f7bbf48d02bff8c5c3a6eb"

INSANE_SKIP:${PN}:append = " already-stripped "
INSANE_SKIP:${PN}:append = " installed-vs-shipped "

SRCREV = "${AUTOREV}"
# ARM6 for pi0w
SRC_URI = " \
    https://github.com/filebrowser/filebrowser/releases/download/v2.23.0/linux-armv6-filebrowser.tar.gz     \
    file://filebrowser.json         \
    file://filebrowser.service      \
"
SRC_URI[sha256sum] = "5d778c349f316b3075135b1da3ad515bea693dd04a2680c92a3a56d3a4436103"

FILES:${PN} = "${systemd_unitdir}/system/filebrowser.service    \
               /usr/bin/filebrowser                             \
               /etc/filebrowser/filebrowser.json       \
               "

do_install() {
    install -d ${D}${bindir}
    install -d ${D}${sysconfdir}
    install -d ${D}/${systemd_unitdir}/system

    mkdir -p ${D}${bindir}
    cp -f ${WORKDIR}/filebrowser ${D}${bindir}
    chmod +x ${D}${bindir}/filebrowser

    mkdir -p ${D}/etc/filebrowser
    cp  ${WORKDIR}/filebrowser.json ${D}/etc/filebrowser

    install -m 0644 ${WORKDIR}/filebrowser.service ${D}/${systemd_unitdir}/system/
}

inherit systemd
SYSTEMD_AUTO_ENABLE = "enable"
SYSTEMD_SERVICE:${PN} = "filebrowser.service"
